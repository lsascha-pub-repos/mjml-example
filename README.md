# Mjml CLI

## Manual Build
### Build Docker Image
```
docker build . -t lsascha/mjml
```

### Run Docker image and convert mjml to html
```
docker run --rm --volume `pwd`:/data lsascha/mjml mjml -r /data/test.mjml -o /data/test.html
```

### Run Interactive Shell
```
docker run -it --rm --volume `pwd`:/data lsascha/mjml /bin/sh
```
