FROM node:16-alpine3.11

RUN apk update && apk add --no-cache nmap && \
    echo @edge http://nl.alpinelinux.org/alpine/edge/community >> /etc/apk/repositories && \
    echo @edge http://nl.alpinelinux.org/alpine/edge/main >> /etc/apk/repositories && \
    apk update

RUN yarn install

RUN yarn add mjml

# adding mjml to bin
ENV PATH $PATH:/node_modules/mjml/bin

RUN adduser -H -D -u 1001 mjml

USER mjml
